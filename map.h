#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

#include <cstdlib>

#define MAP_X_SIZE 10
#define MAP_Y_SIZE 10
#define MAP_Z_SIZE 10

enum {EMPTY, NITRO, TNT, ROCK, WOOD, TEMPLE, BONUS, VERTICAL_WOOD, R_TEMPLE, R_TEMPLE_PAROI, R_TEMPLE_EGYPTIAN_SYMBOL, R_AMULETTE};
enum {ANGLE_0, ANGLE_90, ANGLE_180, ANGLE_270};

typedef struct Cell Cell;
struct Cell
{
    unsigned int object;
    unsigned int orientationSol;
};

void emptyMap(Cell map[MAP_X_SIZE][MAP_Y_SIZE][MAP_Z_SIZE]);
void setCell(Cell map[MAP_X_SIZE][MAP_Y_SIZE][MAP_Z_SIZE], int x, int y, int z, unsigned int object);

#endif // MAP_H_INCLUDED
