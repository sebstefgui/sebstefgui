#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cstdlib>
#include <time.h>

#include "sdlglutils.h"
#include "freeflycamera.h"
#include "map.h"
#include "io.h"

#define FPS 50
#define ROTATION_SPEED 0.05

#define WIDTH_WINDOW 640
#define HEIGHT_WINDOW 480

void Draw(SDL_Window * window, Cell map[MAP_X_SIZE][MAP_Y_SIZE][MAP_Z_SIZE]);
void drawAllCubes(Cell map[MAP_X_SIZE][MAP_Y_SIZE][MAP_Z_SIZE]);
void drawCube(int x, int y, int z, unsigned int object, unsigned int orientationSol);

GLuint textures[NUMBER_OF_TEXTURES]; // Textures identifiants
FreeFlyCamera * camera;

/* double angleZ = 0;
double angleX = 0; */

void stop()
{
    delete camera;
    SDL_Quit();
}

int main(int argc, char *argv[])
{
    const Uint32 time_per_frame = 1000/FPS;
    double znear, zfar;

    // Used for random creations
    srand(time(NULL));
    SDL_Init(SDL_INIT_VIDEO);
    atexit(stop);

    /* Previous SDL 1 code
    SDL_Surface *ecran = NULL;
    SDL_WM_SetCaption("OpenGL application",NULL);
    ecran = SDL_SetVideoMode(WIDTH_WINDOW, HEIGHT_WINDOW, 32, SDL_OPENGL);
    */

    // New SDL 2 code : see http://wiki.libsdl.org/MigrationGuide#Moving_from_SDL_1.2_to_2.0 for more information
    SDL_Window *screen = SDL_CreateWindow("OpenGL application",
                          SDL_WINDOWPOS_CENTERED,
                          SDL_WINDOWPOS_CENTERED,
                          WIDTH_WINDOW, HEIGHT_WINDOW,
                          SDL_WINDOW_OPENGL);
    // Needed for the SDL 2 version when using OpenGL
    SDL_GLContext glcontext = SDL_GL_CreateContext(screen);

    // Writing and reading files
    loadSettings(&znear, &zfar);
    writeOpenGLInfo();

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    gluPerspective(70, (double) WIDTH_WINDOW/HEIGHT_WINDOW, znear, zfar);
    glEnable(GL_DEPTH_TEST); // Needed for the z-buffer
    glEnable(GL_TEXTURE_2D); //Needed for the texturing

    bool keepGoing = true;
    SDL_Event event;

    Uint32 last_time, current_time, elapsed_time, start_time;

    Cell map[MAP_X_SIZE][MAP_Y_SIZE][MAP_Z_SIZE];
    emptyMap(map);

    setCell(map, 6, 0, 3, VERTICAL_WOOD);
    setCell(map, 6, 2, 3, BONUS);
    setCell(map, 6, 4, 1, NITRO);
    setCell(map, 6, 6, 1, TNT);
    setCell(map, 6, 8, 3, WOOD);

    // Creating the temple sample for testing purposes
    setCell(map, 0, 7, 1, R_TEMPLE_PAROI);
    setCell(map, 0, 8, 1, R_TEMPLE_PAROI);
    setCell(map, 1, 9, 1, R_TEMPLE_PAROI);
    setCell(map, 2, 9, 1, R_TEMPLE_PAROI);

    setCell(map, 0, 7, 2, R_TEMPLE_EGYPTIAN_SYMBOL);
    setCell(map, 0, 8, 2, R_TEMPLE);
    setCell(map, 1, 9, 2, R_TEMPLE_EGYPTIAN_SYMBOL);
    setCell(map, 2, 9, 2, R_TEMPLE);

    setCell(map, 0, 7, 3, R_TEMPLE);
    setCell(map, 0, 8, 3, R_AMULETTE);
    setCell(map, 1, 9, 3, R_TEMPLE);
    setCell(map, 2, 9, 3, R_AMULETTE);

    setCell(map, 0, 7, 4, R_AMULETTE);
    setCell(map, 0, 8, 4, R_TEMPLE);
    setCell(map, 1, 9, 4, R_AMULETTE);
    setCell(map, 2, 9, 4, R_TEMPLE);
    // End of sample


    loadTextures(textures);
    camera = new FreeFlyCamera(Vector3D(0.5, -3, 0.5));

    // Last instructions before the main loop
    Draw(screen, map);
    last_time = SDL_GetTicks();
    while (keepGoing)
    {
        start_time = SDL_GetTicks();
        SDL_PollEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                keepGoing = false;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_p:
                        takeScreenshot("test.bmp");
                        break;
                    case SDLK_ESCAPE:
                        keepGoing = false;
                        break;
                    default:
                        camera->OnKeyboard(event.key);
                        break;
                }
                break;
            case SDL_KEYUP:
                camera->OnKeyboard(event.key);
                break;
            case SDL_MOUSEMOTION:
                camera->OnMouseMotion(event.motion);
                break;
            case SDL_MOUSEWHEEL:
                camera->OnMouseWheel(event.wheel);
                break;
            default:
                break;
        }

        current_time = SDL_GetTicks();
        elapsed_time = current_time - last_time;
        last_time = current_time;

        camera->animate(elapsed_time);

        // Should I move these lines to another place?
        /* double angleToBeAdded = ROTATION_SPEED * elapsed_time;
        angleZ += angleToBeAdded;
        angleX += angleToBeAdded; */

        Draw(screen, map);

        // These lines are useful to limit the FPS so that the CPU is way less busy
        elapsed_time = SDL_GetTicks() - start_time;
        if (elapsed_time < time_per_frame)
        {
            // Force the program to sleep until the next frame
            SDL_Delay(time_per_frame - elapsed_time);
        }
    }

    // We delete the context which was created for the SDL 2 code version when using OpenGL
    SDL_GL_DeleteContext(glcontext);
    return EXIT_SUCCESS;
}

void drawAllCubes(Cell map[MAP_X_SIZE][MAP_Y_SIZE][MAP_Z_SIZE])
{
    glPushMatrix();

    for(int x = 0 ; x < MAP_X_SIZE ; x++)
    {
        for (int y = 0 ; y < MAP_Y_SIZE ; y++)
        {
            for (int z = 0 ; z < MAP_Z_SIZE ; z++)
            {
                int object = map[x][y][z].object;
                if(object != EMPTY)
                {
                    drawCube(x, y, z, object, map[x][y][z].orientationSol);
                }
            }
        }
    }

    glPopMatrix();
}

void drawCube(int x, int y, int z, unsigned int object, unsigned int orientationSol)
{
    // Each cube is 1 x 1 x 1 regarding dimensions to make it easier
    // Only for optimization purposes ('p' means plus)
    int xp1 = x + 1, yp1 = y + 1, zp1 = z + 1;

    bool onlyOneTexture = (object == WOOD || object == ROCK || object == TEMPLE);

    int numberTexture;
    switch (object) {
        case NITRO:
            numberTexture = NITRO_SIDES;
            break;
        case ROCK:
            numberTexture = ROCK_ALL;
            break;
        case TNT:
            numberTexture = TNT_SIDES;
            break;
        case WOOD:
            numberTexture = WOOD_ALL;
            break;
        case TEMPLE:
            numberTexture = TEMPLE_FLOOR_ALL;
            break;
        case BONUS:
            numberTexture = BONUS_SIDES;
            break;
        case VERTICAL_WOOD:
            numberTexture = VERTICAL_WOOD_SIDES;
            break;
        case R_TEMPLE:
            numberTexture = R_TEMPLE_SIDES;
            break;
        case R_TEMPLE_PAROI:
            numberTexture = R_TEMPLE_PAROI_SIDES;
            break;
        case R_TEMPLE_EGYPTIAN_SYMBOL:
            numberTexture = R_EGYPTIAN_SYMBOL_SIDES;
            break;
        case R_AMULETTE:
            numberTexture = R_AMULETTE_SIDES;
            break;
        default:
            break;
    }
    glBindTexture(GL_TEXTURE_2D, textures[numberTexture]);

    glBegin(GL_QUADS);

    // Supposing we are at (0, -1, 0) and we are creating the cube looking at (0, 0, 0)
    // In front of
    glTexCoord2d(0, 1);
    glVertex3d(x, y, zp1);
    glTexCoord2d(1, 1);
    glVertex3d(xp1, y, zp1);
    glTexCoord2d(1, 0);
    glVertex3d(xp1, y, z);
    glTexCoord2d(0, 0);
    glVertex3d(x, y, z);

    // On the back
    glTexCoord2d(0, 1);
    glVertex3d(xp1, yp1, zp1);
    glTexCoord2d(1, 1);
    glVertex3d(x, yp1, zp1);
    glTexCoord2d(1, 0);
    glVertex3d(x, yp1, z);
    glTexCoord2d(0, 0);
    glVertex3d(xp1, yp1, z);

    // On the left
    glTexCoord2d(0, 1);
    glVertex3d(x, yp1, zp1);
    glTexCoord2d(1, 1);
    glVertex3d(x, y, zp1);
    glTexCoord2d(1, 0);
    glVertex3d(x, y, z);
    glTexCoord2d(0, 0);
    glVertex3d(x, yp1, z);

    // On the right
    glTexCoord2d(0, 1);
    glVertex3d(xp1, y, zp1);
    glTexCoord2d(1, 1);
    glVertex3d(xp1, yp1, zp1);
    glTexCoord2d(1, 0);
    glVertex3d(xp1, yp1, z);
    glTexCoord2d(0, 0);
    glVertex3d(xp1, y, z);

    // We can access to the next texture like this because we ordered our textures following this logic
    if (!onlyOneTexture)
    {
        glEnd();
        // Work around while waiting for a better solution
        if (object == BONUS || object == VERTICAL_WOOD)
            glBindTexture(GL_TEXTURE_2D, textures[WOOD_ALL]);
        else if (object == R_AMULETTE || object == R_TEMPLE_EGYPTIAN_SYMBOL || object == R_TEMPLE_PAROI)
            glBindTexture(GL_TEXTURE_2D, textures[R_TEMPLE_TOP_BOTTOM]);
        else
            glBindTexture(GL_TEXTURE_2D, textures[numberTexture + 1]);
        glBegin(GL_QUADS);
    }

    // On the bottom
    glTexCoord2d(0, 0);
    glVertex3d(x, yp1, z);
    glTexCoord2d(1, 0);
    glVertex3d(xp1, yp1, z);
    glTexCoord2d(1, 1);
    glVertex3d(xp1, y, z);
    glTexCoord2d(0, 1);
    glVertex3d(x, y, z);

    // On the top
    // TBD think about a better solution for this texture orientation system
    switch (orientationSol)
    {
        case ANGLE_90:
            glTexCoord2d(0, 1);
            glVertex3d(x, y, zp1);
            glTexCoord2d(0, 0);
            glVertex3d(xp1, y, zp1);
            glTexCoord2d(1, 0);
            glVertex3d(xp1, yp1, zp1);
            glTexCoord2d(1, 1);
            glVertex3d(x, yp1, zp1);
            break;
        case ANGLE_180:
            glTexCoord2d(1, 1);
            glVertex3d(x, y, zp1);
            glTexCoord2d(0, 1);
            glVertex3d(xp1, y, zp1);
            glTexCoord2d(0, 0);
            glVertex3d(xp1, yp1, zp1);
            glTexCoord2d(1, 0);
            glVertex3d(x, yp1, zp1);
            break;
        case ANGLE_270:
            glTexCoord2d(1, 0);
            glVertex3d(x, y, zp1);
            glTexCoord2d(1, 1);
            glVertex3d(xp1, y, zp1);
            glTexCoord2d(0, 1);
            glVertex3d(xp1, yp1, zp1);
            glTexCoord2d(0, 0);
            glVertex3d(x, yp1, zp1);
            break;
        case ANGLE_0:
        default:
            glTexCoord2d(0, 0);
            glVertex3d(x, y, zp1);
            glTexCoord2d(1, 0);
            glVertex3d(xp1, y, zp1);
            glTexCoord2d(1, 1);
            glVertex3d(xp1, yp1, zp1);
            glTexCoord2d(0, 1);
            glVertex3d(x, yp1, zp1);
            break;
    }

    glEnd();
}

void Draw(SDL_Window * window, Cell map[MAP_X_SIZE][MAP_Y_SIZE][MAP_Z_SIZE])
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // The second and last flag is useful for using the z-buffer

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );

    camera->look();

    // Should I move these lines to another place?
    // glRotated(angleZ,0,0,1);
    // glRotated(angleX,1,0,0);

    drawAllCubes(map);

    glFlush();
    /*Previous SDL 1 code
    SDL_GL_SwapBuffers();*/
    // New SDL 2 code
    SDL_GL_SwapWindow(window);
}

