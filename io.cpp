#include "io.h"

void loadTextures(GLuint textures[])
{
    textures[NITRO_TOP_BOTTOM] = loadTexture("data/textures/NITRO TOP_BOTTOM.png");
    textures[NITRO_SIDES] = loadTexture("data/textures/NITRO SIDES.png");
    textures[TNT_TOP_BOTTOM] = loadTexture("data/textures/TNT TOP_BOTTOM.png");
    textures[TNT_SIDES] = loadTexture("data/textures/TNT SIDES.png");
    textures[WOOD_ALL] = loadTexture("data/textures/WOOD ALL.png");
    textures[ROCK_ALL] = loadTexture("data/textures/ROCK ALL.png");
    textures[TEMPLE_FLOOR_ALL] = loadTexture("data/textures/TEMPLE_FLOOR ALL.png");
    textures[VERTICAL_WOOD_SIDES] = loadTexture("data/textures/VERTICAL_WOOD SIDES.png");
    textures[BONUS_SIDES] = loadTexture("data/textures/BONUS SIDES.png");
    textures[R_AMULETTE_SIDES] = loadTexture("data/textures/R_AMULETTE SIDES.png");
    textures[R_TEMPLE_SIDES] = loadTexture("data/textures/R_TEMPLE SIDES.png");
    textures[R_TEMPLE_TOP_BOTTOM] = loadTexture("data/textures/R_TEMPLE TOP_BOTTOM.png");
    textures[R_EGYPTIAN_SYMBOL_SIDES] = loadTexture("data/textures/R_EGYPTIAN_SYMBOL SIDES.png");
    textures[R_TEMPLE_PAROI_SIDES] = loadTexture("data/textures/R_TEMPLE_PAROI SIDES.png");
}

void writeOpenGLInfo()
{
    string const fileName("data/files/opengl.info");
    ofstream flux(fileName.c_str());

    if(flux)
    {
        flux << "OpenGL Vendor: " << (char*) glGetString(GL_VENDOR) << endl;
        flux << "OpenGL Renderer: " << (char*) glGetString(GL_RENDERER) << endl;
        flux << "OpenGL Version: " << (char*) glGetString(GL_VERSION) << endl;
    }
}

void loadSettings(double *znear, double *zfar)
{
   *znear = DEFAULT_ZNEAR;
   *zfar = DEFAULT_ZFAR;

   string const fileName("data/files/settings.ini");
   string key, value;
   int pos = 0;

   ifstream flux(fileName.c_str());

   if(flux)
   {
      string line;
      string delimiter = "=";

      while(getline(flux, line))
      {
         if (line.at(0) != '#')
         {
            pos = line.find(delimiter);
            key = line.substr(0, pos);
            value = line.substr(pos + 1, line.length() - (pos + 1));
            if (key.compare("znear") == SAME_STRINGS)
            {
                // C++ method to cast from string to double (I noticed we cannot use stringstream in a row)
                stringstream s;
                s << value;
                s >> *znear;
            }
            else if (key.compare("zfar") == SAME_STRINGS)
            {
                stringstream s;
                s << value;
                s >> *zfar;
            }
         }
      }
   }
}
