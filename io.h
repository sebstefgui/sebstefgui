#ifndef IO_H_INCLUDED
#define IO_H_INCLUDED

#include <GL/gl.h>
#include <GL/glu.h>
#include "sdlglutils.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

// The two following lines (define and enum) need to be updated accordingly
// Notes: to add new textures for a cube that has several textures, you have to make sure you write in the enum:
// First the "sides" texture, then the "top bottom" texture (for optimization purposes)
#define NUMBER_OF_TEXTURES 14
enum {NITRO_SIDES, NITRO_TOP_BOTTOM, TNT_SIDES, TNT_TOP_BOTTOM, WOOD_ALL, ROCK_ALL, TEMPLE_FLOOR_ALL, VERTICAL_WOOD_SIDES, BONUS_SIDES, R_AMULETTE_SIDES, R_TEMPLE_SIDES, R_TEMPLE_TOP_BOTTOM, R_EGYPTIAN_SYMBOL_SIDES, R_TEMPLE_PAROI_SIDES};

#define DEFAULT_ZNEAR 0.1
#define DEFAULT_ZFAR 100.0
#define SAME_STRINGS 0

void loadTextures(GLuint textures[]);
void writeOpenGLInfo();
void loadSettings(double *, double *);

using namespace std;

#endif // IO_H_INCLUDED
