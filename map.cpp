#include "map.h"

void emptyMap(Cell map[MAP_X_SIZE][MAP_Y_SIZE][MAP_Z_SIZE])
{
    for(int x = 0 ; x < MAP_X_SIZE ; x++)
    {
        for (int y = 0 ; y < MAP_Y_SIZE ; y++)
        {
            for (int z = 0 ; z < MAP_Z_SIZE ; z++)
            {
                // We modify this function on purpose only for tests
                if (z == 0)
                    setCell(map, x, y, z, R_TEMPLE);
                else
                    setCell(map, x, y, z, EMPTY);
            }
        }
    }
}

void setCell(Cell map[MAP_X_SIZE][MAP_Y_SIZE][MAP_Z_SIZE], int x, int y, int z, unsigned int object)
{
    map[x][y][z].object = object;
    if (object == ROCK || object == TEMPLE || object == R_TEMPLE)
    {
        map[x][y][z].orientationSol = (rand() % 4);
    }
    else
    {
        map[x][y][z].orientationSol = ANGLE_0;
    }
}
